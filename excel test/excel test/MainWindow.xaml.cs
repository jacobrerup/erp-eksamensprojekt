﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using ExcelDataReader;
using Microsoft.Extensions.DependencyInjection;

namespace excel_test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow ( )
        {
            InitializeComponent();
        }
        public void ConfigureServices ( IServiceCollection services )
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

          
        }

        DataTableCollection tableCollection;
        private void btnOpen_Click ( object sender, RoutedEventArgs e )
        {

            OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Excel Workbook|*.xlsx", ValidateNames = true };

            Nullable<bool> dialogOK = openFileDialog.ShowDialog();
                if (dialogOK == true)
                {
                    txtFilename.Text = openFileDialog.FileName;
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using (var stream = File.Open(openFileDialog.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                                ConfigureDataTable = ( _ ) => new ExcelDataTableConfiguration() { UseHeaderRow = true }
                            });
                            tableCollection = result.Tables;
                            cboSheet.Items.Clear();
                            foreach (DataTable table in tableCollection)
                            {
                                cboSheet.Items.Add(table.TableName);
                            }
                        }
                    }
                }
            
           
                
                
           
            
        }

        

        private void cboSheet_SelectionChanged ( object sender, SelectionChangedEventArgs e )
        {
            DataTable dt = tableCollection[cboSheet.SelectedItem.ToString()];
            dataGrid.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dt });
        }
    }
    

}
